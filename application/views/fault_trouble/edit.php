<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Fault Trouble Edit: <?=$fault_trouble['kategori']?></h3>
            </div>
			<?php echo form_open('fault_trouble/edit/'.$fault_trouble['id_trouble']); ?>
			<div class="box-body">
				<div class="row clearfix">
				
					<div class="col-md-6">
						<label for="trouble" class="control-label">Trouble</label>
						<div class="form-group">
							<input type="text" name="trouble" value="<?php echo ($this->input->post('trouble') ? $this->input->post('trouble') : $fault_trouble['trouble']); ?>" class="form-control" id="trouble" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="sub_trouble" class="control-label">Sub Trouble</label>
						<div class="form-group">
							<input type="text" name="sub_trouble" value="<?php echo ($this->input->post('sub_trouble') ? $this->input->post('sub_trouble') : $fault_trouble['sub_trouble']); ?>" class="form-control" id="sub_trouble" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="countermeasure" class="control-label">Countermeasure</label>
						<div class="form-group">
							<input type="text" name="countermeasure" value="<?php echo ($this->input->post('countermeasure') ? $this->input->post('countermeasure') : $fault_trouble['countermeasure']); ?>" class="form-control" id="countermeasure" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>