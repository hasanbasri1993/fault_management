<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fault Trouble Listing</h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('fault_trouble/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Id Fault</th>
                        <th>Trouble</th>
                        <th>Sub Trouble</th>
                        <th>Countermeasure</th>
                        <th>Last Update</th>
                        <th>Actions</th>
                    </tr>
					<?php $no = 1;
						foreach ($fault_trouble as $f) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $f['kategori']; ?></td>
                                <td><?php echo $f['trouble']; ?></td>
                                <td><?php echo $f['sub_trouble']; ?></td>
                                <td><?php echo $f['countermeasure']; ?></td>
                                <td><?php echo $f['updated_at']; ?></td>
                                <td>
                                    <a href="<?php echo site_url('fault_trouble/edit/' . $f['id_trouble']); ?>"
                                       class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                                    <a href="<?php echo site_url('fault_trouble/remove/' . $f['id_trouble']); ?>"
                                       class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                                </td>
                            </tr>
						<?php $no++;} ?>
                </table>

            </div>
        </div>
    </div>
</div>
