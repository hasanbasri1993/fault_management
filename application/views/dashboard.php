<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?=count($fault_log_all);?></h3>

                    <p>Fault Error</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?=count($fault_log_unsolved);?></h3>

                    <p>Unsolved Case</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>18</h3>

                    <p>Ships</p>
                </div>
                <div class="icon">
                    <i class="fa fa-ship"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
 
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">MAIN ENGINE FAULT MANAGEMENT REPOT</h3>
                    <div class="box-tools pull-right">
                        <input name="start" placeholder="Start Date"/>
                        <input name="end" placeholder="End Date"/>
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-send"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table table-striped" id="main_engine_report">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Ships Name</th>
                                <?php
                                foreach ($fault_category as $s) {
                                    echo '<th>' . $s["kategori"] . '</th>';
                                } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($ship as $item) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $item['nama_kapal']; ?></td>
                                    <td><?= $item['1e']; ?></td>
                                    <td><?= $item['2e']; ?></td>
                                    <td><?= $item['3e']; ?></td>
                                    <td><?= $item['4e']; ?></td>
                                    <td><?= $item['5e']; ?></td>
                                    <td><?= $item['6e']; ?></td>
                                </tr>
                                <?php $no++;
                            } ?>


                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Recently Fault Engine</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table table-striped" id="main_engine_report">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Ships Name</th>
                            <th>Error Category</th>
                            <th>Trouble</th>
                            <th>Countermeasure</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($fault_log as $item) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= date_format(date_create($item['created_at_log']), "l, d F Y H:i:s"); ?></td>
                                <td><?= $item['nama_kapal']; ?></td>
                                <td><?= $item['kategori']; ?></td>
                                <td><?= $item['trouble'] . ' ' . $item['sub_trouble']; ?></td>
                                <td><?= $item['countermeasure']; ?></td>
                                <td><?= $item['status_log'] == 'Y' ? '<span class="btn bg-olive btn-flat">Y</span>' : '<span class="btn bg-maroon btn-flat ">N</span>'; ?></td>
                            </tr>
                            <?php $no++;
                        } ?>


                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="<?= site_url('fault_log'); ?>" class="uppercase">View All Logs</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
</section>
