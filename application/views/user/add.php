<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">User Add</h3>
            </div>
            <?php echo form_open('user/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
                    <div class="col-md-6">
                        <label for="name" class="control-label">Name</label>
                        <div class="form-group">
                            <input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="username" class="control-label">Username</label>
                        <div class="form-group">
                            <input type="text" name="username" value="<?php echo $this->input->post('username'); ?>" class="form-control" id="username" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="ship" class="control-label">Ship</label>
                        <div class="form-group">
                            <select name="ship" class="form-control" >
		                        <?php
			                        foreach ($ship as $row) {
				                        echo "<option value='$row[id_kapal]'>$row[nama_kapal]</option>";
			                        } ?>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
						<label for="password" class="control-label">Password</label>
						<div class="form-group">
							<input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control" id="password" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="level" class="control-label">Level</label>
						<div class="form-group">
                            <select name="level" class="form-control" >
                                <option value="admin">Admin</option>
                                <option value="operator">Operator</option>
                            </select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>