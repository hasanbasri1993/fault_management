<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fault Log Listing</h3>

            </div>
            <div class="box-body">
                <table id="table_fault_log" class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ship Name</th>
                        <th>Error</th>
                        <th>Solved</th>
                        <th>Troubles</th>
                        <th>Counter Measures</th>
                        <th>Notes</th>
                        <th>Operator</th>
                        <th>Created</th>
                        <th>Last Update</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
	                    $no = 1;
	                    foreach ($fault_log as $f) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $f['nama_kapal']; ?></td>
                                <td><?php echo $f['kategori']; ?></td>
                                <td><?php echo $f['status_log'] == 'Y' ? '<span class="btn bg-olive btn-flat">Y</span>' : '<span class="btn bg-maroon btn-flat ">N</span>'; ?></td>
                                <td><?php echo $f['troubles'] != null ? $f['masalah'] : '-'; ?></td>
                                <td><?php echo $f['countermeasures'] != null ? $f['measure'] : '-'; ?></td>
                                <td><?php echo $f['note'] != null ? $f['note'] : '-'; ?></td>
                                <td><?php echo $f['id_user'] != null ? $f['name'] : '-'; ?></td>
                                <td><?php echo date_format(date_create($f['created_at_log']), "l, d F Y H:i:s"); ?></td>
                                <td><?php echo $f['updated_at_log'] != null ? date_format(date_create($f['updated_at_log']), "l, d F Y H:i:s") : '-'; ?></td>
                                <td>
                                    <a href="<?php echo site_url('fault_log/edit/' . $f['log_id']); ?>"
                                       class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                                    <!--<a href="<?php /*echo site_url('fault_log/remove/'.$f['log_id']); */ ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>-->
                                </td>
                            </tr>
		                    <?php
		                    $no++;
	                    } ?>
                    </tbody>
					
                </table>

            </div>
        </div>
    </div>
</div>
