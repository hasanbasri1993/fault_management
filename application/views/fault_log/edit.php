<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Fault Log Assessments </h3>
            </div>
			<?php echo form_open('fault_log/edit/' . $fault_log['id']); ?>
            <div class="box-body">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <label for="id_error" class="control-label">Fault Category</label>
                        <div class="form-group">
                            <select id="id_error" name="id_error" class="form-control" disabled>
                                <option value="">select fault_category</option>
								<?php
									foreach ($all_fault_category as $fault_category) {
										$selected = ($fault_category['id'] == $fault_log['id_error']) ? ' selected="selected"' : "";
										
										echo '<option value="' . $fault_category['id'] . '" ' . $selected . '>' . $fault_category['kategori'] . '</option>';
									}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="id_kapal" class="control-label">Ship</label>
                        <div class="form-group">
                            <select id="id_kapal" name="id_kapal" class="form-control" disabled>
                                <option value="">select ship</option>
								<?php
									foreach ($all_ship as $ship) {
										$selected = ($ship['id_kapal'] == $fault_log['id_kapal']) ? ' selected="selected"' : "";
										
										echo '<option value="' . $ship['id_kapal'] . '" ' . $selected . '>' . $ship['nama_kapal'] . '</option>';
									}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="troubles" class="control-label">Troubles and Countermeasure</label>
                        <div class="form-group">
                            <select id="troubles" name="troubles" class="form-control">
                                <option value="">select trouble</option>
								<?php
									foreach ($all_trouble as $trouble) {
										echo '<option value="' . $trouble['id'] . '">' . $trouble['trouble'] . ' - ' . $trouble['sub_trouble'] . ' - ' . $trouble['countermeasure'] . '</option>';
									}
								?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label for="status" class="control-label">is Solved</label>
                        <div class="form-group">
                            <select id="status" name="status" class="form-control">
                                <option value="Y">Y</option>
                                <option value="N">N</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="note" class="control-label">Note</label>
                        <div class="form-group">
                            <textarea class="form-control" name="note" id="note" rows="5"
                                      placeholder="Find the reason, Please explain the trouble !..."><?= $fault_log['note'];?></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-check"></i> Save
                </button>
            </div>
			<?php echo form_close(); ?>
        </div>
    </div>
</div>