<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ship Add</h3>
            </div>
            <?php echo form_open('ship/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="nama_kapal" class="control-label">Nama Kapal</label>
						<div class="form-group">
							<input type="text" name="nama_kapal" value="<?php echo $this->input->post('nama_kapal'); ?>" class="form-control" id="nama_kapal" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>