<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Ship Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('ship/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>No </th>
						<th>Ship Name </th>
						<th>Actions</th>
                    </tr>
                    <?php
                        $no = 1;
                        foreach($ship as $s){
                        
                        ?>
                    <tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $s['nama_kapal']; ?></td>
						<td>
                            <a href="<?php echo site_url('ship/edit/'.$s['id_kapal']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                            <a href="<?php echo site_url('ship/remove/'.$s['id_kapal']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php $no++;} ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
