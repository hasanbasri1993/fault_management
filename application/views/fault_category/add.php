<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Fault Category Add</h3>
            </div>
            <?php echo form_open('fault_category/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="kategori" class="control-label">Kategori</label>
						<div class="form-group">
							<input type="text" name="kategori" value="<?php echo $this->input->post('kategori'); ?>" class="form-control" id="kategori" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="set_point" class="control-label">Set Point</label>
						<div class="form-group">
							<input type="text" name="set_point" value="<?php echo $this->input->post('set_point'); ?>" class="form-control" id="set_point" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="satuan" class="control-label">Satuan</label>
						<div class="form-group">
							<input type="text" name="satuan" value="<?php echo $this->input->post('satuan'); ?>" class="form-control" id="satuan" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="effect" class="control-label">Effect</label>
						<div class="form-group">
							<input type="text" name="effect" value="<?php echo $this->input->post('effect'); ?>" class="form-control" id="effect" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>