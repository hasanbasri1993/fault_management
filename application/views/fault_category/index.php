<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fault Category Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('fault_category/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Kategori</th>
						<th>Set Point</th>
						<th>Satuan</th>
						<th>Effect</th>
						<th>Actions</th>
                    </tr>
                    <?php
                        $no=1;
                        foreach($fault_category as $f){ ?>
                    <tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $f['kategori']; ?></td>
						<td><?php echo $f['set_point']; ?></td>
						<td><?php echo $f['satuan']; ?></td>
						<td><?php echo $f['effect']; ?></td>
						<td>
                            <a href="<?php echo site_url('fault_category/edit/'.$f['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('fault_category/remove/'.$f['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php $no++;} ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
