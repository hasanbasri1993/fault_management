<?php
	/*
	 * Generated by CRUDigniter v3.2
	 * www.crudigniter.com
	 */
	
	class Fault_log_model extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		/*
		 * Get fault_log by id
		 */
		function get_fault_log($id)
		{
			return $this->db->get_where('fault_log', array('id' => $id))->row_array();
		}
		
		/*
		 * Get all fault_log
		 */
		function get_all_fault_log($limit = 5, $unsolved = false)
		{
			$this->db->select('*');
			$this->db->select('CONCAT(fault_trouble.trouble, " ", fault_trouble.sub_trouble) AS masalah');
			$this->db->select('fault_trouble.countermeasure as measure');
			$this->db->select('fault_log.id as log_id');
			$this->db->select('fault_log.status as status_log');
			$this->db->select('fault_log.updated_at as updated_at_log');
			$this->db->select('fault_log.created_at as created_at_log');
			$this->db->order_by('fault_log.id', 'desc');
			if ($limit)
				$this->db->limit($limit);
			$this->db->join('fault_trouble', 'fault_trouble.id = fault_log.troubles', 'left');
			$this->db->join('users', 'users.id = fault_log.id_user', 'left');
			$this->db->join('ship', 'ship.id_kapal = fault_log.id_kapal', 'left');
			$this->db->join('fault_category', 'fault_log.id_error = fault_category.id', 'left');
			if ($unsolved)
				$this->db->where('fault_log.status', 'N');
			return $this->db->get('fault_log')->result_array();
		}
		
		function get_summary_log(){
			$this->db->select("nama_kapal");
			$this->db->select(",IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=1 AND a.id_kapal=ship.id_kapal),0) '1e',IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=2 AND a.id_kapal=ship.id_kapal),0) '2e',IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=3 AND a.id_kapal=ship.id_kapal),0) '3e',IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=4 AND a.id_kapal=ship.id_kapal),0) '4e',IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=5 AND a.id_kapal=ship.id_kapal),0) '5e',IFNULL((
SELECT COUNT(a.id_error) FROM fault_log a WHERE a.id_error=6 AND a.id_kapal=ship.id_kapal),0) '6e'");
			return $this->db->get('ship')->result_array();
		}
		
		/*
		 * function to add new fault_log
		 */
		function add_fault_log($params)
		{
			$this->db->insert('fault_log', $params);
			return $this->db->insert_id();
		}
		
		/*
		 * function to update fault_log
		 */
		function update_fault_log($id, $params)
		{
			$this->db->where('id', $id);
			return $this->db->update('fault_log', $params);
		}
		
		/*
		 * function to delete fault_log
		 */
		function delete_fault_log($id)
		{
			return $this->db->delete('fault_log', array('id' => $id));
		}
	}
