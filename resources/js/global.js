$(document).ready(function () {

    if ($('.has-datetimepicker').length) {
        $('.has-datetimepicker').datetimepicker();
    }

    if ($('.has-datepicker').length) {
        $('.has-datepicker').datetimepicker({format: 'DD/MM/YYYY'});
    }

    if ($('#table_fault_log').length) {
        $('#table_fault_log').DataTable();
    }
    if ($('#main_engine_report').length) {
        $('#main_engine_report').DataTable({
            "paging":   false,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ]
        });
    }

});